package com.maxdev;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.shape.*;
import javafx.scene.paint.Color;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Lab1");
        Group mygroup = new Group();
        Scene scene = new Scene (mygroup, 420, 300);

        Rectangle tvbody = new Rectangle(90, 75, 220, 150);
        mygroup.getChildren().add(tvbody);
        tvbody.setFill(Color.rgb(254, 165, 0));

        Rectangle tvScreen = new Rectangle(100, 85, 150, 130);
        tvScreen.setArcHeight(25);
        tvScreen.setArcWidth(25);
        mygroup.getChildren().add(tvScreen);
        tvScreen.setFill(Color.rgb(127, 127, 127));

        Circle c1 = new Circle(290.0, 150.0,6.0);
        mygroup.getChildren().add(c1);
        c1.setFill(Color.rgb(0, 0, 0));

        Circle c2 = new Circle(290.0, 175.0,6.0);
        mygroup.getChildren().add(c2);
        c1.setFill(Color.rgb(0, 0, 0));

        Circle c3 = new Circle(290.0, 200.0,6.0);
        mygroup.getChildren().add(c3);
        c1.setFill(Color.rgb(0, 0, 0));

        Line antena1 = new Line(210.0, 75.0, 170.0, 35.0);
        mygroup.getChildren().add(antena1);
        antena1.setStroke(Color.BLACK);
        antena1.setStrokeWidth(1.0);

        Line antena2 = new Line(210.0, 75.0, 250.0, 35.0);
        mygroup.getChildren().add(antena2);
        antena1.setStroke(Color.BLACK);
        antena1.setStrokeWidth(1.0);

        scene.setFill(Color.rgb(127, 254, 0));

        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
