package lab;

import java.awt.Container;
import javax.media.j3d.*; // for transform
import javax.vecmath.Color3f;
import java.awt.Color;
import com.sun.j3d.utils.geometry.*;
import javax.vecmath.*; // for Vector3f
import com.sun.j3d.utils.image.TextureLoader;

public class SnowmanBody {
	public static Sphere getSphereBody(float radius) {   
		int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;   
		return new Sphere(radius, primflags, getXMassTreeAppearence());  
		} 
	 
	private static Appearance getXMassTreeAppearence() {  
		Appearance ap = new Appearance();   
		Color3f emissive = new Color3f(0.1f, 0.1f, 0.1f);   
		Color3f ambient = new Color3f(0.7f, 0.7f, 0.7f);   
		Color3f diffuse = new Color3f(1f, 1f, 1f);   
		Color3f specular = new Color3f(1f, 1f, 1f);   
		ap.setMaterial(new Material(ambient, emissive, diffuse, specular, 1.0f));   
		return ap;  
	}
	
	public static Sphere getSphere(float radius) {   
		int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;   
		return new Sphere(radius, primflags, getAppearence());  
		} 
	 
	private static Appearance getAppearence() {  
		Appearance ap = new Appearance();   
		Color3f emissive = new Color3f(0.1f, 0.1f, 0.1f);   
		Color3f ambient = new Color3f(0.1f, 0.1f, 0.1f);   
		Color3f diffuse = new Color3f(0.1f, 0.1f, 0.1f);   
		Color3f specular = new Color3f(0.1f, 0.1f, 0.1f);   
		ap.setMaterial(new Material(ambient, emissive, diffuse, specular, 1.0f));   
		return ap;  
	}
}
