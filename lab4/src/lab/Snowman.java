package lab;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.SimpleUniverse;
import javax.media.j3d.*;
import javax.swing.Timer;
import javax.vecmath.*;

// Vector3f - float, Vector3d - double
public class Snowman implements ActionListener {
	
	private float upperEyeLimit = 5.0f; // 5.0
    private float lowerEyeLimit = 1.0f; // 1.0
    private float farthestEyeLimit = 6.0f; // 6.0
    private float nearestEyeLimit = 3.0f; // 3.0
    private TransformGroup viewingTransformGroup;
    private Transform3D viewingTransform = new Transform3D();
    private float eyeHeight;
    private float eyeDistance;
    private boolean descend = true;
    private boolean approaching = true;

	private TransformGroup treeTransformGroup;  
	private Transform3D treeTransform3D = new Transform3D();  
	private Timer timer;  
	private float angle = 0; 
	 
	public static void main(String[] args) {   
		new Snowman();  
	} 
	 
	 public Snowman() {   
		 timer = new Timer(50, this);   
		 timer.start();   
		 BranchGroup scene = createSceneGraph();   
		 SimpleUniverse u = new SimpleUniverse();   
		 u.getViewingPlatform().setNominalViewingTransform();   
		 u.addBranchGraph(scene);  
	} 
	 
	 public BranchGroup createSceneGraph() { 
	 
	  // ��������� ����� ��'����   
		 BranchGroup objRoot = new BranchGroup(); 
	 
	  // ��������� ��'���, �� ������ �������� �� �����   
		 treeTransformGroup = new TransformGroup();   
		 treeTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);   
		 buildCastleSkeleton();   
		 objRoot.addChild(treeTransformGroup);
	 
	  // ����������� ���������   
		 BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),100.0);   
		 Color3f light1Color = new Color3f(1.0f, 0.5f, 0.4f);   
		 Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);   
		 DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);   
		 light1.setInfluencingBounds(bounds);   objRoot.addChild(light1); 
	 
	  // ������������ ���������� ���������   
		 Color3f ambientColor = new Color3f(1.0f, 1.0f, 1.0f);   
		 AmbientLight ambientLightNode = new AmbientLight(ambientColor);   
		 ambientLightNode.setInfluencingBounds(bounds);   
		 objRoot.addChild(ambientLightNode);   return objRoot;  
	}

    private void buildCastleSkeleton() {
    	TransformGroup tgTop = new TransformGroup();   
    	Transform3D transformTop = new Transform3D();   
    	Sphere topSphere = SnowmanBody.getSphereBody(0.2f);  
    	Vector3f vectorTop = new Vector3f(.0f, 0.5f, .0f);   
    	transformTop.setTranslation(vectorTop);   
    	tgTop.setTransform(transformTop);   
    	tgTop.addChild(topSphere);   
    	treeTransformGroup.addChild(tgTop); 
    	 
    	TransformGroup tgMiddle = new TransformGroup();   
    	Transform3D transformMiddle = new Transform3D();   
    	Sphere midSphere = SnowmanBody.getSphereBody(0.3f);  
    	Vector3f vectorMiddle = new Vector3f(.0f, .1f, .0f);   
    	transformMiddle.setTranslation(vectorMiddle);   
    	tgMiddle.setTransform(transformMiddle);   
    	tgMiddle.addChild(midSphere);   
    	treeTransformGroup.addChild(tgMiddle); 

    	TransformGroup tgBottom = new TransformGroup();   
    	Transform3D transformBottom = new Transform3D();   
    	Sphere botSphere = SnowmanBody.getSphereBody(0.4f);   
    	Vector3f vectorBottom = new Vector3f(.0f, -0.4f, .0f);   
    	transformBottom.setTranslation(vectorBottom);   
    	tgBottom.setTransform(transformBottom);   
    	tgBottom.addChild(botSphere);   
    	treeTransformGroup.addChild(tgBottom);
    	
    	TransformGroup tg1 = new TransformGroup();   
    	Transform3D transform1 = new Transform3D();   
    	Sphere cone1 = SnowmanBody.getSphere(.03f);   
    	Vector3f vector1 = new Vector3f(.18f, 0.55f, .1f);   
    	transform1.setTranslation(vector1);   
    	tg1.setTransform(transform1);   
    	tg1.addChild(cone1);   
    	treeTransformGroup.addChild(tg1);
    	
    	TransformGroup tg2 = new TransformGroup();   
    	Transform3D transform2 = new Transform3D();   
    	Sphere cone2 = SnowmanBody.getSphere(.03f);   
    	Vector3f vector2 = new Vector3f(.18f, 0.55f, -.1f);   
    	transform2.setTranslation(vector2);   
    	tg2.setTransform(transform2);   
    	tg2.addChild(cone2);   
    	treeTransformGroup.addChild(tg2);
    	
    	TransformGroup tg3 = new TransformGroup();   
    	Transform3D transform3 = new Transform3D();   
    	Sphere cone3 = SnowmanBody.getSphere(.03f);   
    	Vector3f vector3 = new Vector3f(.2f, 0.5f, 0f);   
    	transform3.setTranslation(vector3);   
    	tg3.setTransform(transform3);   
    	tg3.addChild(cone3);   
    	treeTransformGroup.addChild(tg3);
    	
    	
    }
    @Override
    public void actionPerformed(ActionEvent e) {
    	treeTransform3D.rotY(angle);   
    	treeTransformGroup.setTransform(treeTransform3D);   
    	angle += 0.05; 
    }
}
